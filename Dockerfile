FROM golang:1.10 as builder

WORKDIR $GOPATH/src/prometheus-exporter-demo
COPY . .

RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o main .
RUN go install -v


FROM index.alauda.cn/alaudaorg/alaudabase:alpine-supervisor-migrate-1

WORKDIR /demo

COPY --from=builder /go/src/prometheus-exporter-demo .

RUN chmod +x /demo/main
EXPOSE 8888

CMD ["/demo/main"]
